# RappiMovies-Erika



## Ejercicio Técnico
La siguiente prueba está diseñada para evaluar su conocimiento y experiencia como
desarrollador Android. Por favor tenga en cuenta lo siguiente:
- Consumir el API de películas de la siguiente pagina:
https://developers.themoviedb.org/
- Se comparte el siguiente diseño como guía, el candidato puede utilizar el diseño de
su gusto:
https://www.figma.com/file/x4r17vtNpB6hQgZhm7F0Tw/Prueba-RappiPay-%C2%B7-
Devp?node-id=0%3A1
- Se deben mostrar 2 categorías: Próximos estrenos(upcoming) y Tendencia(top
rated), el scroll debe ser horizontal por categoría, no es necesario manejar
paginación
- Manejar apartado Recomendados para ti utilizando las películas Tendencia para
filtrar por Idioma y Año de lanzamiento (a elección del candidato), se debe mostrar
en forma de cuadrícula limitando a solo 6 películas
- Visualizar detalle de la película seleccionada
Puntos importantes:
1. Opción de ver trailer en el detalle (cómo visualizar el trailer queda a elección del
candidato).
2. La app debe poder funcionar offline (cache).
3. Transiciones/Animaciones.
4. Uso de buenas prácticas y escalabilidad.
5. Pruebas Unitarias.
Responda y escriba dentro de un archivo Readme las siguientes preguntas:
1. ¿En qué consiste el principio de responsabilidad única? 
    nos dice que un módulo tiene una única razón para cambiar
¿Cuál es su propósito?
    es una herramienta indispensable para proteger nuestro código frente a cambios, ya que implica que sólo debería haber un motivo por el que modificar una clase.
2. ¿Qué características tiene, según su opinión, un “buen” código o código limpio?
    es enfocado, no debe ser redundante, expresivo, extensible por otro desarrollador, facil de leer el codigo.
3. Detalla cómo harías todo aquello que no hayas llegado a completar.
    no quedo nada pendiente. Muchas Gracias
Entregable:
- Link de Github/Bitbucket/Gitlab/Otro o archivo comprimido del proyecto
- Versión para instalar en dispositivos
