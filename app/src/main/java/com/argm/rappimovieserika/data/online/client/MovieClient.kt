package com.argm.rappimovieserika.data.online.client

import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.model.MoviesModel
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.http.GET

interface MovieClient {
    @GET(Constants.URL_MOVIES_UPCOMING)
    suspend fun getMoviesUpcoming(): Response<MoviesModel>

    @GET(Constants.URL_MOVIES_TOP_RATED)
    suspend fun getMoviesTopRated(): Response<MoviesModel>

    @GET(Constants.URL_MOVIES_YEAR)
    suspend fun getMoviesYear(): Response<MoviesModel>

    @GET(Constants.URL_MOVIES_LANGUAGE)
    suspend fun getMoviesLanguage(): Response<MoviesModel>
}