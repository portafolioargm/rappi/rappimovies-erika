package com.argm.rappimovieserika.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class VideoModel(
    val iso_639_1:String?,
    val iso_3166_1: String?,
    val name:String?,
    val key:String?,
    val site:String?,
    val size:String?,
    val type:String?,
    val official:Boolean?,
    val published_at:String?,
    val id:String?
):Parcelable