package com.argm.rappimovieserika.data.model

data class VideosModel (
    val id : Int?,
    val results : List<VideoModel>
)