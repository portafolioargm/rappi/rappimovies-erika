package com.argm.rappimovieserika.data.offline.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.argm.rappimovieserika.data.offline.entity.GeneroEntity

@Dao
interface GeneroDao {

    @Query("SELECT * FROM genero_table")
    suspend fun getAllGenerosDB(): List<GeneroEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertGenerosOnlyTypeDB(movies: List<GeneroEntity>)

    @Delete
    suspend fun deleteGenerosOnlyTypeDB(movies: List<GeneroEntity>)
}