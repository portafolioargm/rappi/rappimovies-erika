package com.argm.rappimovieserika.data.online.client

import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.model.GenerosModel
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.http.GET

interface GeneroClient {
    @GET(Constants.URL_GENRES)
    suspend fun getGenres(): Response<GenerosModel>
}