package com.argm.rappimovieserika.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class GeneroModel(
    val id: Int?,
    val name: String?,
):Parcelable