package com.argm.rappimovieserika.data.model

data class MoviesModel (
    val page : Int?,
    val results : List<MovieModel>
)