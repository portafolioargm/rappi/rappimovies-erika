package com.argm.rappimovieserika.data.repository

import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.offline.dao.MovieDao
import com.argm.rappimovieserika.data.offline.entity.MovieEntity
import com.argm.rappimovieserika.data.online.ApiService
import com.argm.rappimovieserika.domain.model.Movie
import com.argm.rappimovieserika.domain.model.toDomain
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val api: ApiService,
    private val dao: MovieDao
) {
    fun getMoviesFromApi(type_movies: Int?): Flow<List<Movie>> = flow {
        when (type_movies) {
            Constants.TYPE_MOVIES_UPCOMING -> {
                api.getMoviesUpcoming().collect {
                    emit(
                        it.results.map { result ->
                            result.toDomain(type_movies)
                        }
                    )
                }
            }

            Constants.TYPE_MOVIES_TOP_RATED -> {
                api.getMoviesTopRated().collect {
                    emit(
                        it.results.map { result ->
                            result.toDomain(type_movies)
                        }
                    )
                }
            }

            Constants.TYPE_MOVIES_LANGUAGE -> {
                api.getMoviesForLanguage().collect {
                    emit(
                        it.results.map { result ->
                            result.toDomain(type_movies)
                        }
                    )
                }
            }

            Constants.TYPE_MOVIES_YEAR -> {
                api.getMoviesYear().collect { results ->
                    emit(
                        results.results.map {
                            it.toDomain(type_movies)
                        }
                    )
                }
            }

            else -> {
                api.getMoviesUpcoming().collect { results ->
                    emit(
                        results.results.map {
                            it.toDomain(type_movies!!)
                        }
                    )
                }
            }
        }
    }

    fun getMoviesFromDB(type_movies: Int?): Flow<List<Movie>> = flow {
        emit(
            try {
                (dao.getAllMoviesDB(type_movies!!)).map { it.toDomain(type_movies!!) }
            } catch (e: Exception) {
                emptyList()
            }
        )
    }

    suspend fun insertMoviesDB(movies: List<MovieEntity>) {
        dao.insertMoviesOnlyTypeDB(movies)
    }

    /*suspend fun removeMoviesDB(movies: List<MovieEntity>){
        dao.deleteMoviesOnlyTypeDB(movies)
    }*/
    suspend fun removeMoviesForTypeDB(type: Int) {
        dao.deleteForTypeDB(type)
    }
}