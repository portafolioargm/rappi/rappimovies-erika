package com.argm.rappimovieserika.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class GenerosModel(
    val genres : List<GeneroModel>
):Parcelable