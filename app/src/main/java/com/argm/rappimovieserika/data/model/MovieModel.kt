package com.argm.rappimovieserika.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieModel(
    val vote_count: Int?,
    val id: Int?,
    val video: Boolean?,
    val vote_average: Float?,
    val title:String?,
    val name: String?,
    val popularity:Float?,
    val poster_path:String?,
    val original_language: String?,
    val original_title:String?,
    val original_name:String?,
    val backdrop_path: String?,
    val adult: Boolean?,
    val overview: String?,
    val release_date: String?,
    val first_air_date: String?,
    val genre_ids: List<Int>?,
):Parcelable