package com.argm.rappimovieserika.data.offline.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.argm.rappimovieserika.domain.model.Genero

@Entity(tableName = "genero_table")
data class GeneroEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="indice") val indice: Int = 0,
    @ColumnInfo(name="name") val name: String?,
    @ColumnInfo(name="id") val id: Int?,
)

fun Genero.toDataBase() = GeneroEntity(
    id=id,
    name=name
)