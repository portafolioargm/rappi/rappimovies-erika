package com.argm.rappimovieserika.data.offline.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.argm.rappimovieserika.data.offline.entity.VideoEntity

@Dao
interface VideoDao {

    @Query("SELECT * FROM video_table WHERE id_movie= :idMovie")
    suspend fun getAllVideosDB(idMovie: Int?): List<VideoEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertVideosOnlyTypeDB(videos: List<VideoEntity>)

    @Query("DELETE FROM video_table WHERE id_movie= :idMovie")
    suspend fun deleteVideosMovieDB(idMovie: Int?)

}