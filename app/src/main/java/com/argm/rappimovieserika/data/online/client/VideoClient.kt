package com.argm.rappimovieserika.data.online.client

import com.argm.rappimovieserika.data.model.VideosModel
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url


interface VideoClient {
    @GET()
    suspend fun getVideosForId(@Url url: String?): Response<VideosModel>
}