package com.argm.rappimovieserika.data.offline

import androidx.room.Database
import androidx.room.RoomDatabase
import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.offline.dao.GeneroDao
import com.argm.rappimovieserika.data.offline.dao.MovieDao
import com.argm.rappimovieserika.data.offline.dao.VideoDao
import com.argm.rappimovieserika.data.offline.entity.GeneroEntity
import com.argm.rappimovieserika.data.offline.entity.MovieEntity
import com.argm.rappimovieserika.data.offline.entity.VideoEntity

@Database(entities = [MovieEntity::class, VideoEntity::class, GeneroEntity::class], version = Constants.VERSION_SQLITE)
abstract class DataBase: RoomDatabase() {
     abstract fun getMoviesDao(): MovieDao
     abstract fun getGenerosDao(): GeneroDao
     abstract fun getVideosForIdMovie(): VideoDao
}