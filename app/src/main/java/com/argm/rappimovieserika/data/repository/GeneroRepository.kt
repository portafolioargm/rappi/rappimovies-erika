package com.argm.rappimovieserika.data.repository

import com.argm.rappimovieserika.data.offline.dao.GeneroDao
import com.argm.rappimovieserika.data.offline.entity.GeneroEntity
import com.argm.rappimovieserika.data.online.ApiService
import com.argm.rappimovieserika.domain.model.Genero
import com.argm.rappimovieserika.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GeneroRepository  @Inject constructor(
    private val api : ApiService,
    private val dao : GeneroDao
){
    fun getGenresFromApi(): Flow<List<Genero>> = flow {
        try {
            api.getGenres().collect{genres ->
            emit(genres.genres.map {
                    it.toDomain()
                })
            }
        }catch (e: Exception){
            emit(emptyList())
        }
    }

    suspend fun getGenresFromDB(): List<Genero> {
        return try {
            (dao.getAllGenerosDB()).map { it.toDomain() }
        }catch (e: Exception){
            emptyList()
        }
    }

    suspend fun insertGenresDB(items: List<GeneroEntity>){
        dao.insertGenerosOnlyTypeDB(items)
    }

    suspend fun removeGenresDB(items: List<GeneroEntity>){
        dao.deleteGenerosOnlyTypeDB(items)
    }
}