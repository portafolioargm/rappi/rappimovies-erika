package com.argm.rappimovieserika.data.online

import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.model.GeneroModel
import com.argm.rappimovieserika.data.model.GenerosModel
import com.argm.rappimovieserika.data.model.MovieModel
import com.argm.rappimovieserika.data.model.MoviesModel
import com.argm.rappimovieserika.data.model.VideoModel
import com.argm.rappimovieserika.data.model.VideosModel
import com.argm.rappimovieserika.data.online.client.GeneroClient
import com.argm.rappimovieserika.data.online.client.MovieClient
import com.argm.rappimovieserika.data.online.client.VideoClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiService @Inject constructor(
    private val apiMovie: MovieClient,
    private val apiVideo: VideoClient,
    private val apiGeneros: GeneroClient
) {
    /**
     * Obtener las peliculas mas populares
     */
    fun getMoviesUpcoming(): Flow<MoviesModel> = flow {
        val response = apiMovie.getMoviesUpcoming()
        emit(response.body() ?: MoviesModel(0, emptyList()))
    }

    /**
     * Obtener las peliculas en tendencia
     */
    fun getMoviesTopRated(): Flow<MoviesModel> = flow {
        val response = apiMovie.getMoviesTopRated()
        emit(response.body() ?: MoviesModel(0, emptyList()))
    }

    /**
     * Obtener las peliculas en Recien Reproducidas
     */
    fun getMoviesYear(): Flow<MoviesModel> = flow {
        val response = apiMovie.getMoviesYear()
        emit(response.body() ?: MoviesModel(0, emptyList()))
    }

    /**
     * Obtener las peliculas en Populares
     */
    fun getMoviesForLanguage(): Flow<MoviesModel> = flow {
        val response = apiMovie.getMoviesLanguage()
        emit(response.body() ?: MoviesModel(0, emptyList()))
    }

    /**
     * Obtener las Generos
     */
    fun getGenres(): Flow<GenerosModel> = flow {
        val response = apiGeneros.getGenres()
        emit(response.body() ?: GenerosModel(emptyList()))
    }

    /**
     * Obtener los videos que pertenecen a un ID
     */
    fun getVideosForID(idMovie: Int): Flow<VideosModel> = flow {
        var url = Constants.URL_GET_VIDEO
        url = url.replace(Constants.KEY_REPLACE_GET_VIDEO, idMovie.toString())
        val response = apiVideo.getVideosForId(url)
        emit(response.body() ?: VideosModel(0, emptyList()))
    }
}