package com.argm.rappimovieserika.data.offline.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.argm.rappimovieserika.domain.model.Video

@Entity(tableName = "video_table")
data class VideoEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="indice") val indice: Int = 0,
    @ColumnInfo(name="iso_639_1") val iso_639_1:String?,
    @ColumnInfo(name="iso_3166_1") val iso_3166_1: String?,
    @ColumnInfo(name="name") val name:String?,
    @ColumnInfo(name="key") val key:String?,
    @ColumnInfo(name="site") val site:String?,
    @ColumnInfo(name="size") val size:String?,
    @ColumnInfo(name="type") val type:String?,
    @ColumnInfo(name="official") val official:Boolean?,
    @ColumnInfo(name="published_at") val published_at:String?,
    @ColumnInfo(name="id") val id:String?,
    @ColumnInfo(name="id_movie") val id_movie:Int?
)

fun Video.toDataBase(idMovie: Int) = VideoEntity(
    iso_639_1=iso_639_1,
    iso_3166_1=iso_3166_1,
    name=name,
    key=key,
    site=site,
    size=size,
    type=type,
    official=official,
    published_at=published_at,
    id=id,
    id_movie=idMovie
)