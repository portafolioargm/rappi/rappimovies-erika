package com.argm.rappimovieserika.data.repository

import com.argm.rappimovieserika.data.offline.dao.VideoDao
import com.argm.rappimovieserika.data.offline.entity.VideoEntity
import com.argm.rappimovieserika.data.online.ApiService
import com.argm.rappimovieserika.domain.model.Video
import com.argm.rappimovieserika.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class VideoRepository  @Inject constructor(
    private val api : ApiService,
    private val dao : VideoDao
    ){
    fun getVideosForIdApi(idMovie: Int): Flow<List<Video>> = flow {
        try {
            api.getVideosForID(idMovie).collect{
                emit(it.results.map { video -> video.toDomain(idMovie) })
            }
        }catch (e: Exception){
            emit (emptyList())
        }
    }

    fun getVideosForIdDB(idMovie: Int): Flow<List<Video>> = flow {
        try {
            emit (dao.getAllVideosDB(idMovie).map { it.toDomain(idMovie) })
        }catch (e: Exception){
            emit (emptyList())
        }
    }

    suspend fun insertVideosDB(videos: List<VideoEntity>){
        dao.insertVideosOnlyTypeDB(videos)
    }

    suspend fun removeVideosDB(idMovie: Int){
        dao.deleteVideosMovieDB(idMovie)
    }
}