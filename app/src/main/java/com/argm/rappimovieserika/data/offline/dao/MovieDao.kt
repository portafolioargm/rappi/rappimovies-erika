package com.argm.rappimovieserika.data.offline.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.argm.rappimovieserika.data.offline.entity.MovieEntity

@Dao
interface MovieDao {

    @Query("SELECT * FROM movie_table WHERE type=:type")
    suspend fun getAllMoviesDB(type: Int): List<MovieEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMoviesOnlyTypeDB(movies: List<MovieEntity>)

    @Delete
    suspend fun deleteMoviesOnlyTypeDB(movies: List<MovieEntity>)

    @Query("DELETE FROM movie_table WHERE type=:type")
    suspend fun deleteForTypeDB(type: Int)
}