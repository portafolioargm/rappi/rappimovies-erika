package com.argm.rappimovieserika.domain.usercase
import com.argm.rappimovieserika.data.offline.entity.toDataBase
import com.argm.rappimovieserika.data.repository.GeneroRepository
import com.argm.rappimovieserika.domain.model.Genero
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GeneroCU @Inject constructor(private val repository : GeneroRepository) {
    operator fun invoke(): Flow<List<Genero>> = flow {
        repository.getGenresFromApi().collect{
            if (it.isNotEmpty()){
                repository.removeGenresDB(it.map { genre-> genre.toDataBase() })
                repository.insertGenresDB(it.map { genre-> genre.toDataBase() })
                emit(it)
            }else{
                emit(repository.getGenresFromDB())
            }
        }
    }
}
