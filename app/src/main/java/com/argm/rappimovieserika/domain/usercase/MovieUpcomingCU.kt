package com.argm.rappimovieserika.domain.usercase

import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.offline.entity.toDataBase
import com.argm.rappimovieserika.data.repository.MovieRepository
import com.argm.rappimovieserika.domain.model.Movie
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieUpcomingCU @Inject constructor(private val repository: MovieRepository) {
    operator fun invoke(): Flow<List<Movie>> = flow {
        repository.getMoviesFromApi(Constants.TYPE_MOVIES_UPCOMING).collect {
            if (it.isNotEmpty()) {
                repository.removeMoviesForTypeDB(Constants.TYPE_MOVIES_UPCOMING)
                repository.insertMoviesDB(it.map { movie -> movie.toDataBase(Constants.TYPE_MOVIES_UPCOMING) })
                emit(it)
            } else {
                repository.getMoviesFromDB(Constants.TYPE_MOVIES_UPCOMING).collect { movies ->
                    emit(movies)
                }
            }
        }
    }
}
