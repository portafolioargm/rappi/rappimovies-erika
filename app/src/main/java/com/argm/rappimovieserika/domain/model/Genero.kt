package com.argm.rappimovieserika.domain.model

import android.os.Parcelable
import com.argm.rappimovieserika.data.model.GeneroModel
import com.argm.rappimovieserika.data.offline.entity.GeneroEntity
import kotlinx.parcelize.Parcelize

@Parcelize
data class Genero(
    val id: Int?,
    val name: String?,
) : Parcelable

fun GeneroModel.toDomain() = Genero(
    id,
    name
)

fun GeneroEntity.toDomain() = Genero(
    id,
    name
)
