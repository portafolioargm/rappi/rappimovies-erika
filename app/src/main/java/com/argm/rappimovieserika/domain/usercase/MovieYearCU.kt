package com.argm.rappimovieserika.domain.usercase
import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.offline.entity.toDataBase
import com.argm.rappimovieserika.data.repository.MovieRepository
import com.argm.rappimovieserika.domain.model.Movie
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MovieYearCU @Inject constructor(private val repository : MovieRepository) {
    operator fun invoke(): Flow<List<Movie>> = flow {
        repository.getMoviesFromApi(Constants.TYPE_MOVIES_YEAR).collect{
            if (it.isNotEmpty()){
                repository.removeMoviesForTypeDB(Constants.TYPE_MOVIES_YEAR)
                repository.insertMoviesDB(it.map {  movie -> movie.toDataBase(Constants.TYPE_MOVIES_YEAR) })
                emit(it)
            }else{
                repository.getMoviesFromDB(Constants.TYPE_MOVIES_YEAR).collect{ movies ->
                    emit(movies)
                }
            }
        }
    }
}
