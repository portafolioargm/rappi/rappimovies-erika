package com.argm.rappimovieserika.domain.usercase;

import com.argm.rappimovieserika.data.offline.entity.toDataBase
import com.argm.rappimovieserika.data.repository.VideoRepository
import com.argm.rappimovieserika.domain.model.Video
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class VideoCU @Inject constructor(private val repository : VideoRepository) {
    operator fun invoke(idMovie: Int): Flow<List<Video>> = flow {
        repository.getVideosForIdApi(idMovie).collect {
            if (it.isNotEmpty()){
                repository.removeVideosDB(idMovie)
                repository.insertVideosDB(it.map {video -> video.toDataBase(idMovie) })
                emit(it)
            }else{
                repository.getVideosForIdDB(idMovie).collect {videos->
                    emit(videos)
                }
            }
        }
    }
}
