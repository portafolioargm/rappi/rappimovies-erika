package com.argm.rappimovieserika

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RappiMoviesErikaApp: Application() {
}