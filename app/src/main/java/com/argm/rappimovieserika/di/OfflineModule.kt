package com.argm.rappimovieserika.di

import android.content.Context
import androidx.room.Room
import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.offline.DataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object OfflineModule {
    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, DataBase::class.java, Constants.NAME_DB)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()

    @Singleton
    @Provides
    fun provideMovieDao(db: DataBase) = db.getMoviesDao()

    @Singleton
    @Provides
    fun provideVideoDao(db: DataBase) = db.getVideosForIdMovie()

    @Singleton
    @Provides
    fun provideGenresDao(db: DataBase) = db.getGenerosDao()
}