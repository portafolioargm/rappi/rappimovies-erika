package com.argm.rappimovieserika.di

import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.online.client.GeneroClient
import com.argm.rappimovieserika.data.online.client.MovieClient
import com.argm.rappimovieserika.data.online.client.VideoClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object OnlineModule {
    //@Singleton Mantiene un patron de una unica instancia
    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit{
        return Retrofit.Builder()
            .baseUrl(Constants.URL_API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideMovieClient(retrofit: Retrofit): MovieClient {
        return retrofit.create(MovieClient::class.java)
    }

    @Singleton
    @Provides
    fun provideVideoClient(retrofit: Retrofit): VideoClient {
        return retrofit.create(VideoClient::class.java)
    }

    @Singleton
    @Provides
    fun provideGenresClient(retrofit: Retrofit): GeneroClient {
        return retrofit.create(GeneroClient::class.java)
    }
}