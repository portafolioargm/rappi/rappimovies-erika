package com.argm.rappimovieserika.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.rappimovieserika.domain.model.Genero
import com.argm.rappimovieserika.domain.usercase.GeneroCU
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GeneroViewModel @Inject constructor(
    private val cu : GeneroCU
) : ViewModel() {
    val uiStates = MutableStateFlow(UiStates(items = emptyList()))
    fun onCreate() {
        viewModelScope.launch {
            val result = cu()
            result.collect{
                if (it.isNotEmpty()) {
                    uiStates.value = UiStates(items = it)
                }
            }
        }
    }

    data class UiStates(
        val items: List<Genero> = emptyList()
    )
}