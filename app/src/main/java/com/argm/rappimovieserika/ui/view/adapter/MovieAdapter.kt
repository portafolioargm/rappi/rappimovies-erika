package com.argm.rappimovieserika.ui.view.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.transition.Transition
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.RecyclerView
import com.argm.rappimovieserika.R
import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.core.unsafeOkHttpClinet
import com.argm.rappimovieserika.databinding.PosterBinding
import com.argm.rappimovieserika.domain.model.Movie
import com.argm.rappimovieserika.ui.view.activity.DetailActivity
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import okhttp3.OkHttpClient
import android.util.Pair as UtilPair


class MovieAdapter(private val movies: List<Movie>) :
    RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    var ctx: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        ctx = parent.context
        return MovieViewHolder(inflater.inflate(R.layout.poster, parent, false))
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = movies[position]
        holder.render(item)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    class MovieViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val binding = PosterBinding.bind(view)

        fun render(movie: Movie) {
            val picassoClient: OkHttpClient = unsafeOkHttpClinet.unsafeOkHttpClient
            val picasso = Picasso.Builder(binding.imgPoster.context)
                .downloader(OkHttp3Downloader(picassoClient)).build()
            picasso.isLoggingEnabled = true;
            picasso
                .load(Constants.URL_PATH_POSTER + movie.poster_path)
                .placeholder(R.color.black)
                .resize(180, 250)
                .centerCrop()
                .into(binding.imgPoster, object : Callback {
                    override fun onSuccess() {
                        binding.imgPoster.scaleType = ImageView.ScaleType.CENTER_CROP
                    }

                    override fun onError(e: Exception?) {
                        Log.e("Antovict", e!!.toString())
                        binding.imgPoster.scaleType = ImageView.ScaleType.CENTER
                        binding.imgPoster.setImageResource(R.mipmap.ic_launcher_round)
                    }
                })
            binding.imgPoster.setOnClickListener(View.OnClickListener {
                val detail = Intent(binding.imgPoster.context, DetailActivity::class.java)
                detail.putExtra("movie", movie)
                detail.putExtra("img", binding.imgPoster.drawable.toBitmap())
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    binding.imgPoster.context as Activity, binding.imgPoster, "imgPoster"
                )
                (binding.imgPoster.context as Activity).startActivity(detail, options.toBundle())
            })
        }
    }
}