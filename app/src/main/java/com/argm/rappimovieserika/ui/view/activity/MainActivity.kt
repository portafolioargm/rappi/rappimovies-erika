package com.argm.rappimovieserika.ui.view.activity

import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.argm.rappimovieserika.R
import com.argm.rappimovieserika.databinding.ActivityHome01Binding
import com.argm.rappimovieserika.ui.view.adapter.MovieAdapter
import com.argm.rappimovieserika.ui.viewModel.MovieLanguageViewModel
import com.argm.rappimovieserika.ui.viewModel.MovieTopRatedViewModel
import com.argm.rappimovieserika.ui.viewModel.MovieUpcomingViewModel
import com.argm.rappimovieserika.ui.viewModel.MovieYearViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHome01Binding
    private lateinit var view: View
    private val movieTopRatedViewModel: MovieTopRatedViewModel by viewModels()
    private val movieUpcomingViewModel: MovieUpcomingViewModel by viewModels()
    private val movieLanguageViewModel: MovieLanguageViewModel by viewModels()
    private val movieYearViewModel: MovieYearViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHome01Binding.inflate(layoutInflater)
        view = binding.root
        setContentView(view)
        movieTopRatedViewModel.onCreate()
        movieUpcomingViewModel.onCreate()
        movieYearViewModel.onCreate()
        movieLanguageViewModel.onCreate()

        lifecycleScope.launch {
            movieLanguageViewModel.uiStates.collect {
                binding.progressRecomendadas.isVisible = it.isLoading
            }
        }

        lifecycleScope.launch {
            movieTopRatedViewModel.uiStates.collect {
                binding.progressRated.isVisible = it.isLoading
                binding.rvPopulares.adapter = MovieAdapter(it.movies)
            }
        }

        lifecycleScope.launch {
            movieUpcomingViewModel.uiStates.collect {
                binding.progressEstrenos.isVisible = it.isLoading
                binding.rvEstrenos.adapter = MovieAdapter(it.movies)
            }
        }

        lifecycleScope.launch {
            movieYearViewModel.uiStates.collect {
                binding.progressRecomendadas.isVisible = it.isLoading
            }
        }

        binding.rgButton.setOnCheckedChangeListener { radioGroup, i ->
            when (radioGroup.checkedRadioButtonId) {
                R.id.rbYear -> {
                    binding.rbYear.setTypeface(
                        Typeface.createFromAsset(
                            assets,
                            "fonts/poppins.ttf"
                        ), Typeface.BOLD
                    );
                    binding.rbLanguage.setTypeface(
                        Typeface.createFromAsset(
                            assets,
                            "fonts/poppins.ttf"
                        ), Typeface.NORMAL
                    );

                    binding.rbYear.setTextColor(
                        ContextCompat.getColor(
                            view.context,
                            R.color.black
                        )
                    )
                    binding.rbLanguage.setTextColor(
                        ContextCompat.getColor(
                            view.context,
                            R.color.white
                        )
                    )
                    lifecycleScope.launch {
                        movieYearViewModel.uiStates.collect {
                            binding.progressRecomendadas.isVisible = it.isLoading
                            binding.rvRecomendadas.adapter = MovieAdapter(it.movies)
                        }
                    }
                }

                R.id.rbLanguage -> {
                    binding.rbYear.setTypeface(
                        Typeface.createFromAsset(
                            assets,
                            "fonts/poppins.ttf"
                        ), Typeface.NORMAL
                    );
                    binding.rbLanguage.setTypeface(
                        Typeface.createFromAsset(
                            assets,
                            "fonts/poppins.ttf"
                        ), Typeface.BOLD
                    );

                    binding.rbYear.setTextColor(
                        ContextCompat.getColor(
                            view.context,
                            R.color.white
                        )
                    )
                    binding.rbLanguage.setTextColor(
                        ContextCompat.getColor(
                            view.context,
                            R.color.black
                        )
                    )
                    lifecycleScope.launch {
                        movieYearViewModel.uiStates.collect {
                            binding.progressRecomendadas.isVisible = it.isLoading
                            binding.rvRecomendadas.adapter = MovieAdapter(it.movies)
                        }
                    }
                }

                else -> {
                    binding.rbYear.setTypeface(
                        Typeface.createFromAsset(
                            assets,
                            "fonts/poppins.ttf"
                        ), Typeface.NORMAL
                    );
                    binding.rbLanguage.setTypeface(
                        Typeface.createFromAsset(
                            assets,
                            "fonts/poppins.ttf"
                        ), Typeface.BOLD
                    );

                    binding.rbYear.setTextColor(
                        ContextCompat.getColor(
                            view.context,
                            R.color.white
                        )
                    )
                    binding.rbLanguage.setTextColor(
                        ContextCompat.getColor(
                            view.context,
                            R.color.black
                        )
                    )
                    lifecycleScope.launch {
                        movieYearViewModel.uiStates.collect {
                            binding.progressRecomendadas.isVisible = it.isLoading
                            binding.rvRecomendadas.adapter = MovieAdapter(it.movies)
                        }
                    }
                }
            }
        }
        binding.rgButton.check(binding.rbLanguage.id)
    }
}