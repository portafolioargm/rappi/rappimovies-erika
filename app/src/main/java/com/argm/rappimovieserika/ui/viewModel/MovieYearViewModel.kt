package com.argm.rappimovieserika.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.rappimovieserika.domain.model.Movie
import com.argm.rappimovieserika.domain.usercase.MovieYearCU
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieYearViewModel @Inject constructor(
    private val movieCU : MovieYearCU
) : ViewModel() {
    val uiStates = MutableStateFlow(UiStates(
        movies = emptyList(),
        isLoading = false
    ))

    fun onCreate() {
        viewModelScope.launch {
            uiStates.value = UiStates(isLoading = true, movies = emptyList())
            val result = movieCU()
            result.collect {
                if (it.isNotEmpty()) {
                    uiStates.value = UiStates(isLoading = false, movies = it.subList(0, 6))
                }else{
                    uiStates.value = UiStates(isLoading = false, movies = emptyList())
                }
            }
        }
    }
    data class UiStates(
        val movies: List<Movie> = emptyList(),
        val isLoading: Boolean = false
    )
}