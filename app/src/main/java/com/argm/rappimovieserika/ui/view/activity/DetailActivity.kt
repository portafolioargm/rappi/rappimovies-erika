package com.argm.rappimovieserika.ui.view.activity

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.graphics.drawable.toDrawable
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.argm.rappimovieserika.R
import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.core.unsafeOkHttpClinet
import com.argm.rappimovieserika.databinding.ActivityDetailBinding
import com.argm.rappimovieserika.domain.model.Genero
import com.argm.rappimovieserika.domain.model.Movie
import com.argm.rappimovieserika.domain.model.Video
import com.argm.rappimovieserika.ui.viewModel.GeneroViewModel
import com.argm.rappimovieserika.ui.viewModel.VideoViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.squareup.picasso.Callback
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient


@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    private lateinit var binding: ActivityDetailBinding
    private val videoViewModel: VideoViewModel by viewModels()
    private val generoViewModel: GeneroViewModel by viewModels()
    private lateinit var videoReproducir : Video
    private var tieneVideo: Boolean = false
    private lateinit var generos : List<Genero>

    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val bundle = intent.extras
        val movie = bundle?.getParcelable<Movie>("movie")!!
        val img = bundle.getParcelable<Bitmap>("img")!!
        videoViewModel.onCreate(movie.id!!)
        generoViewModel.onCreate()

        lifecycleScope.launch {
            generoViewModel.uiStates.collect{
                generos = it.items
                val genre_ids : List<Int> = stringToArrayInt(movie.genre_ids)
                binding.layoutAnimation.txtGeneros.text = null
                for (i in genre_ids.indices){
                    val name : String? = searchGeneroForId(genre_ids[i], generos).name
                    if (name != ""){
                        if (binding.layoutAnimation.txtGeneros.text.isEmpty()){
                            binding.layoutAnimation.txtGeneros.text = name
                        }else{
                            binding.layoutAnimation.txtGeneros.text = binding.layoutAnimation.txtGeneros.text.toString().plus(Constants.SEPARATOR).plus(name)
                        }
                    }
                }
            }
        }

        bottomSheetBehavior = BottomSheetBehavior.from(binding.layoutAnimation.contentInfo)

        bottomSheetBehavior.peekHeight = binding.layoutAnimation.contentPreInfo.height
        binding.layoutAnimation.contentPreInfo.post {
            bottomSheetBehavior.peekHeight = binding.layoutAnimation.contentPreInfo.height
        }

        // change the state of the bottom sheet
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

        // set callback for changes
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED){
                    binding.layoutAnimation.btnExpand.setImageResource(R.drawable.ic_arrow_up)
                }else if (newState == BottomSheetBehavior.STATE_EXPANDED){
                    binding.layoutAnimation.btnExpand.setImageResource(R.drawable.ic_arrow_down)
                }else{
                    binding.layoutAnimation.btnExpand.setImageResource(R.drawable.ic_horizontal_rule)
                }
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })
        binding.imgPoster.setImageBitmap(img)
        val picassoClient: OkHttpClient = unsafeOkHttpClinet.unsafeOkHttpClient
        val picasso = Picasso.Builder(binding.imgPoster.context).downloader(OkHttp3Downloader(picassoClient)).build()
        picasso.isLoggingEnabled = true;
        picasso
            .load(Constants.URL_PATH_POSTER + movie.poster_path)
            .placeholder(binding.imgPoster.drawable)
            .resize(360, 500)
            .centerCrop()
            .into(binding.imgPoster, object : Callback {
                override fun onSuccess() {

                }

                override fun onError(e: Exception?) {
                    Log.e("Antovict", e!!.message.toString())
                    binding.imgPoster.scaleType = ImageView.ScaleType.CENTER
                    binding.imgPoster.setImageResource(R.mipmap.ic_launcher_round)
                }
            })
        binding.layoutAnimation.chipAnio.text = movie.release_date!!.substring(0, 4)
        binding.layoutAnimation.chipIdioma.text = movie.original_language
        binding.layoutAnimation.chipPuntuacion.text = movie.vote_average.toString()
        binding.layoutAnimation.txtTitle.text = movie.title
        binding.layoutAnimation.txtTitleDescription.text = movie.original_title
        binding.layoutAnimation.Description.text = movie.overview
        binding.btnBack.setOnClickListener {
            onBackPressed()
        }

        binding.layoutAnimation.btnExpand.setOnClickListener(View.OnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED){
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
            else{
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        })

        lifecycleScope.launch {
            videoViewModel.uiStates.collect {

                var videos: List<Video> = it.videos
                for ( i in videos.indices){
                    if (videos[i].site?.uppercase().equals("YOUTUBE")){
                        videoReproducir = videos[i]
                        tieneVideo = true
                        break
                    }
                }
                binding.layoutAnimation.btnVer.setOnClickListener(View.OnClickListener {
                    if (tieneVideo){
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(Constants.URL_YOUTUBE + videoReproducir.key)
                        view.context.startActivity(intent)
                    }else{
                        Toast.makeText(view.context, "No tiene Trailer", Toast.LENGTH_LONG).show()
                    }

                })
            }
        }

    }

    private fun stringToArrayInt(mnemonic: String): List<Int> {
        val words = ArrayList<Int>()
        for (word in mnemonic.trim { it <= ' ' }.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
            if (word.isNotEmpty()) {
                Log.i("INDICE", word)
                words.add(word.trim().toInt())
            }
        }
        return words
    }

    private fun searchGeneroForId(id: Int, list: List<Genero>): Genero{
        for (i in list.indices){
            if (list[i].id == id){
                return list[i]
            }
        }
        return Genero(0, "")
    }
}