package com.argm.rappimovieserika.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.rappimovieserika.domain.model.Video
import com.argm.rappimovieserika.domain.usercase.VideoCU
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VideoViewModel @Inject constructor(
    private val videoUseCase : VideoCU
) : ViewModel() {
    val uiStates = MutableStateFlow(
        UiStates(
            videos = emptyList(),
            isLoading = false
        )
    )

    fun onCreate(idMovie: Int) {
        viewModelScope.launch {
            uiStates.value = UiStates(isLoading = true, videos = emptyList())
            val result = videoUseCase(idMovie)
            result.collect {
                if (it.isNotEmpty()) {
                    uiStates.value = UiStates(isLoading = false, videos = it)
                }else{
                    uiStates.value = UiStates(isLoading = false, videos = emptyList())
                }
            }
        }
    }
    data class UiStates(
        val videos: List<Video> = emptyList(),
        val isLoading: Boolean = false
    )
}