package com.argm.rappimovieserika.core
object Constants {

    //DOMINIO
    const val URL_API = "https://api.themoviedb.org/3/"
    //DB
    const val VERSION_SQLITE = 2
    const val NAME_DB = "rappi_movies"

    //API
    const val API_KEY = "37a97ca4fc0278ad1e04fc3b857f1dec"
    const val API_KEY_YOUTUBE = "AIzaSyCCM4_3c2bsVbWndY0UeE0kZVxWprSIEIA"
    const val API_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzN2E5N2NhNGZjMDI3OGFkMWUwNGZjM2I4NTdmMWRlYyIsInN1YiI6IjViODU4Mzc4OTI1MTQxNTlkNzA1M2RiYSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Yeivtod1c0TpeKWM0__ANASRa_ewhR9VLKFgqVl7zOY"

    //URLs
    const val URL_MOVIES_TOP_RATED = URL_API + "movie/top_rated?api_key=$API_KEY"
    const val URL_MOVIES_UPCOMING = URL_API + "movie/upcoming?api_key=$API_KEY"
    const val URL_MOVIES_LANGUAGE = URL_API + "discover/movie?api_key=$API_KEY&with_original_language=es&language=es-ES"
    const val URL_MOVIES_YEAR = URL_API + "discover/movie?api_key=$API_KEY&primary_release_year=2013"
    const val URL_GENRES = URL_API + "genre/movie/list?api_key=$API_KEY"
    const val URL_GET_VIDEO = URL_API + "movie/{movie_id}/videos?api_key=$API_KEY"
    const val KEY_REPLACE_GET_VIDEO = "{movie_id}"
    const val URL_PATH_POSTER = "https://image.tmdb.org/t/p/w600_and_h900_bestv2/"
    const val URL_YOUTUBE = "https://www.youtube.com/watch?v="

    //COMUN
    const val TYPE_MOVIES_TOP_RATED = 0
    const val TYPE_MOVIES_UPCOMING = 1
    const val TYPE_MOVIES_LANGUAGE = 2
    const val TYPE_MOVIES_YEAR = 3

    const val SEPARATOR = " • "
}