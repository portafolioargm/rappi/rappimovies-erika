package com.argm.rappimoviesnew.domain

import com.argm.rappimovieserika.data.repository.GeneroRepository
import com.argm.rappimovieserika.domain.model.Genero
import com.argm.rappimovieserika.domain.usercase.GeneroCU
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GeneroCUTest{
    @RelaxedMockK
    private lateinit var repository : GeneroRepository

    lateinit var generoUseCase: GeneroCU
    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        generoUseCase = GeneroCU(repository)
    }

    @Test
    fun `cuando el api no retorna nada devuelve los valores de la DB para Videos`() = runBlocking {
        //Given
        coEvery { repository.getGenresFromApi() } returns emptyList()
        //When
        generoUseCase()
        //Then
        coVerify(exactly = 1) { repository.getGenresFromDB() }
    }

    @Test
    fun `cuando el api retorna un listado de videos`() = runBlocking {
        val myList = listOf(Genero(1, "Drama"))
        //Given
        coEvery { repository.getGenresFromApi() } returns myList
        //When
        val response = generoUseCase()
        //Then
        coVerify(exactly = 1) { repository.removeGenresDB(any()) }
        coVerify(exactly = 1) { repository.insertGenresDB(any()) }
        coVerify(exactly = 0) { repository.getGenresFromDB() }
        assert(myList==response)
    }
}