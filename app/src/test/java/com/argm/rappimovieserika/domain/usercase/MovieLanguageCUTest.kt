package com.argm.rappimoviesnew.domain

import com.argm.rappimovieserika.core.Constants
import com.argm.rappimovieserika.data.repository.MovieRepository
import com.argm.rappimovieserika.domain.model.Movie
import com.argm.rappimovieserika.domain.usercase.MovieLanguageCU
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class MoviePopularUseCaseTest{
    @RelaxedMockK
    private lateinit var repository : MovieRepository

    lateinit var movieLanguageUseCase: MovieLanguageCU
    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        movieLanguageUseCase = MovieLanguageCU(repository)
    }

    @Test
    fun `cuando el api no retorna nada devuelve los valores de la DB para Movies Idioma`() = runBlocking {
        //Given
        coEvery { repository.getMoviesFromApi(Constants.TYPE_MOVIES_LANGUAGE) } returns emptyList()
        //When
        movieLanguageUseCase()
        //Then
        coVerify(exactly = 1) { repository.getMoviesFromDB(Constants.TYPE_MOVIES_LANGUAGE) }
    }

    @Test
    fun `cuando el api retorna un listado de movies Idioma`() = runBlocking {
        val myList = listOf(Movie(10, 1, false, 10.0F, "Prueba Movie", "Pelicula", 5.0F, "prueba_poster", "ES", "Prueba Original Movie", "Pelicula Original", null, false, "Descripcion de la pelicula en general", "10-12-2021", "24-12-2021", Constants.TYPE_MOVIES_LANGUAGE, "1, 2"))
        //Given
        coEvery { repository.getMoviesFromApi(Constants.TYPE_MOVIES_LANGUAGE) } returns myList
        //When
        val response = movieLanguageUseCase()
        //Then
        coVerify(exactly = 1) { repository.removeMoviesForTypeDB(any()) }
        coVerify(exactly = 1) { repository.insertMoviesDB(any()) }
        coVerify(exactly = 0) { repository.getMoviesFromDB(Constants.TYPE_MOVIES_LANGUAGE) }
        assert(myList==response)
    }
}