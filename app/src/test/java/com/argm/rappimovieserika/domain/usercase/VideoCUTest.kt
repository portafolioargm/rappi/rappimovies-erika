package com.argm.rappimoviesnew.domain

import com.argm.rappimovieserika.data.repository.VideoRepository
import com.argm.rappimovieserika.domain.model.Video
import com.argm.rappimovieserika.domain.usercase.VideoCU
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class VideoCUTest{
    @RelaxedMockK
    private lateinit var repository : VideoRepository

    lateinit var videoUseCase: VideoCU
    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        videoUseCase = VideoCU(repository)
    }

    @Test
    fun `cuando el api no retorna nada devuelve los valores de la DB para Videos`() = runBlocking {
        //Given
        coEvery { repository.getVideosForIdApi(any()) } returns emptyList()
        //When
        videoUseCase(1)
        //Then
        coVerify(exactly = 1) { repository.getVideosForIdDB(any()) }
    }

    @Test
    fun `cuando el api retorna un listado de videos`() = runBlocking {
        val myList = listOf(Video("en", "US", "Video Trailer", "JfVOs4VSpmA", "Youtube", "1080", "Trailer", true, "2021-11-17T01:30:05.000Z", "61945b8a4da3d4002992d5a6", 1))
        //Given
        coEvery { repository.getVideosForIdApi(any()) } returns myList
        //When
        val response = videoUseCase(1)
        //Then
        coVerify(exactly = 1) { repository.removeVideosDB(any()) }
        coVerify(exactly = 1) { repository.insertVideosDB(any()) }
        coVerify(exactly = 0) { repository.getVideosForIdDB(any()) }
        assert(myList==response)
    }
}